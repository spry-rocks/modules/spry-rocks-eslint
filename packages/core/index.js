module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb-typescript/base',
    'prettier'
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['@typescript-eslint', 'prettier', 'import'],
  rules: {
    'import/no-unresolved': 'off',
    'import/named': 'off',
    'import/order': 'off',
    'import/prefer-default-export': 'off',
    'no-shadow': 'off',
    'default-case': 'off',
    'consistent-return': 'off',
    'prettier/prettier': ['error', require('@spryrocks/prettier-config')],
    'no-console': 'error',
    'no-restricted-syntax': 'off',
    '@typescript-eslint/ban-ts-comment': 'error',
    '@typescript-eslint/no-explicit-any': 'error',
    'no-underscore-dangle': 'off',
    '@typescript-eslint/naming-convention': ['error', {
      'selector': 'typeLike',
      'format': ['PascalCase'],
    }],
    '@typescript-eslint/no-unused-vars': ['error', {'argsIgnorePattern': '^_'}],
    'func-names': 'off',
    '@typescript-eslint/no-shadow': 'off',
    'sort-imports': ['error', {
      'allowSeparatedGroups': true,
      "ignoreCase": true,
    }],
    'no-await-in-loop': 'off',
    "no-redeclare": "off",
    "@typescript-eslint/no-redeclare": "off"
  }
};
