module.exports = {
    'singleQuote': true,
    'bracketSameLine': false,
    'trailingComma': 'all',
    'arrowParens': 'always',
    'bracketSpacing': false,
    'printWidth': 90,
}
